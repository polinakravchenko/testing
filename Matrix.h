#pragma once
#ifndef __MATRIX_LIB__

#define __MATRIX_LIB__

#include "MatrixExceptions.h"

#include <iostream>

namespace matrix {

	class Matrix {
	private:
		double** data;
		size_t rows;
		size_t cols;
		void init(const size_t& rows, const size_t& cols);
	protected:
		void clear();
		void check_cell(const size_t&, const size_t&) const;
	public:
		Matrix(const size_t& rows = 3, const size_t& cols = 3);
		virtual ~Matrix();

		const double& get(const size_t&, const size_t&) const;

		double& get(const size_t&, const size_t&);

		const double& operator()(const size_t&, const size_t&) const;

		double& operator()(const size_t&, const size_t&);

		friend std::ostream& operator<<(std::ostream&, const Matrix&);
	};
}
#endif //__MATRIX_LIB__