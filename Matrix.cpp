﻿// Matrix.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "Matrix.h"
#include <cmath>
#include <ostream>

using std::pow;
using std::sin;
using std::abs;

namespace matrix {
	void Matrix::init(const size_t& rows, const size_t& cols) {
		data = new double* [rows];
		for (size_t i = 0; i < rows; ++i) {
			data[i] = new double[cols];
		}
	}

	void Matrix::clear() {
		for (size_t i = 0; i < rows; ++i) {
			if (data[i]) {
				delete[] data[i];
			}
		}
		delete[] data;
	}

	void Matrix::check_cell(const size_t& row, const size_t& col) const
	{
		if (row < 0 || row >= rows || col < 0 || col >= cols)
		{
			throw xInvalidCell(row, col);
		}
	}

	Matrix::Matrix(const size_t& rows_, const size_t& cols_)
		: rows(rows_)
		, cols(cols_)
	{
		init(rows_, cols_);
	}

	Matrix::~Matrix() {
		clear();
	}

	const double& Matrix::get(const size_t& row, const size_t& col) const
	{
		check_cell(row, col);
		return data[row][col];
	}

	double& Matrix::get(const size_t& row, const size_t& col)
	{
		check_cell(row, col);
		return data[row][col];
	}

	const double& Matrix::operator()(const size_t& row, const size_t& col) const
	{
		return get(row, col);
	}


	double& Matrix::operator()(const size_t& row, const size_t& col)
	{
		return get(row, col);
	}

	std::ostream& operator<<(std::ostream& os, const Matrix& m) {
		for (size_t row = 0; row < m.rows; ++row) {
			for (size_t col = 0; col < m.cols; ++col) {
				os << m.get(row, col) << (col == m.cols - 1 ? "\n" : "\t");
			}
		}
		return os;
	}
}
